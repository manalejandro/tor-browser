# tor-browser

This repo defines the `tor-browser` Docker image updated and cloned from [hkjn](https://github.com/hkjn/tor-browser).

## Install:

- Need `docker`

```
./build.sh
```

## Usage:

You can run tor-browser in a container with a command like:

```
./tor_browser.sh

or

docker run -it --rm --name tor-browser \
           -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
           -e DISPLAY=unix$DISPLAY \
           tb
```

If you want to specify a custom `torrc` file, that can be done with:
```
docker run -it --rm --name tor-browser \
           -v /host/dir/containing/torrc:/conf:ro \
           -e TORRC_PATH=/conf/torrc \
           -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
           -e DISPLAY=unix$DISPLAY \
           tb
```

## Pull fresh image directly from our registry.gitlab.com:
```
docker run -it --rm --name tor-browser \
           -v /host/dir/containing/torrc:/conf:ro \
           -e TORRC_PATH=/conf/torrc \
           -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
           -e DISPLAY=unix$DISPLAY \
           registry.gitlab.com/manalejandro/tor-browser:latest
```
